'use strict';

angular.
  module('productTable').
  component('productTable', {
    templateUrl: './app/product-table/product-table.template.html',
    controller: function($scope, productService, currencyMethods) {
      var that = this;
      $scope.sortType = 'name';
      $scope.sortReverse = false;
      $scope.currencyQuery = 'NZD';
      $scope.products = productService.requestData();

      this.changeCurrency = function() {
        $scope.products.forEach(function(ele) {
          ele.convertedPrice = currencyMethods.changeCurrency($scope.currencyQuery, ele.price);
          ele.currencySymbol = currencyMethods.changeSymbol($scope.currencyQuery);
        });
      };

      this.changeCurrency();
    }
  });
