var productTable = angular.module('productTable', []);

productTable.service('currencyMethods', function() {
  this.changeSymbol = function(currency) {
    switch (currency) {
      case 'EURO':
        return '€';
        break;
      default:
        return '$';
    }
  },
  this.changeCurrency = function(currency, price) {
    if ('number' !== typeof price ) {
      return false;
    }

    if (price > 0) {
      switch (currency) {
        case 'USD':
          return Number.parseInt((price * 0.76).toFixed(2));
          break;
        case 'EURO':
          return Number.parseInt((price * 0.67).toFixed(2));
          break;
        default:
          return Number.parseInt(price.toFixed(2));
      }
    }
    return false;
  }
}).service('productService', function() {
  this.requestData = function() {
    var lawnMowers = new LawnmowerRepository().getAll();
    lawnMowers.forEach(function(ele) {
      ele.type = "Lawnmower";
    });

    var phoneCases = new PhoneCaseRepository().getAll();
    phoneCases.forEach(function(ele) {
      ele.type = "Phone Case";
    });

    var tShirts = new TShirtRepository().getAll();
    tShirts.forEach(function(ele) {
      ele.type = "T-Shirt";
    });

    return lawnMowers.concat(phoneCases, tShirts);
  }
});;
