'use strict';

describe('myApp', function() {

  it('has a Hello World! test', function() {
    expect("Hello" + " world!").toEqual("Hello world!");
  });

  describe('currencyMethods service', function() {
    var currencyMethods;

    beforeEach(function(){
      module('app');
      inject(function($injector){
        currencyMethods = $injector.get('currencyMethods');
      });
    });

    it('loads the "currencyMethods" from the "product-table" module', function(){
      expect(currencyMethods).toBeDefined();
    });

    it('has a changeSymbol method', function() {
      expect(currencyMethods.changeSymbol()).toBeDefined();
    });

    it('has a changeCurrency method', function() {
      expect(currencyMethods.changeCurrency()).toBeDefined();
    });

    describe('changeCurrency method', function() {

      it('takes a number and converts to NZD', function() {
        const nzdPrice = 100.00;
        const changeCurrency = currencyMethods.changeCurrency("NZD", nzdPrice);
        expect(changeCurrency).toEqual(100.00);
      });

      it('takes a number and converts to USD', function() {
        // USD is 0.76 : 1.00 NZD
        const nzdPrice = 100.00;
        const changeCurrency = currencyMethods.changeCurrency("USD", nzdPrice);
        expect(changeCurrency).toEqual(76.00);
      });

      it('takes a number and converts to EURO', function() {
        // EURO is 0.67 : 1.00 NZD
        const nzdPrice = 100.00;
        const changeCurrency = currencyMethods.changeCurrency("EURO", nzdPrice);
        expect(changeCurrency).toEqual(67.00);
      });

      it ('will not take alphabet characters', function() {
        const price = "abc";
        const changeCurrency = currencyMethods.changeCurrency("NZD", price);
        expect(changeCurrency).toBeFalsy();
      });

      it ('must take positive numbers', function() {
        const price = -99.00;
        const changeCurrency = currencyMethods.changeCurrency("NZD", price);
        expect(changeCurrency).toBeFalsy();
      })

      it ('must take properly formatted numbers', function() {
        const price = "1ab0c0a.00";
        const changeCurrency = currencyMethods.changeCurrency("NZD", price);
        expect(changeCurrency).toBeFalsy();
      });

    });
  });

  describe('productService service', function() {
    var productService;

    beforeEach(function(){
      module('app');
      inject(function($injector){
        productService = $injector.get('productService');
      });
    });

    it('loads the "productService" from the "product-table" module', function(){
      expect(productService).toBeDefined();
    });

    it('returns an array of length >= 1', function() {
      var products = productService.requestData();
      expect(products.length).toBeGreaterThanOrEqual(1);
    });
  });

  describe('product-table controller', function() {

    var $componentController, products;

    beforeEach(module('productTable'));

    beforeEach(inject(function(_$componentController_) {
      $componentController = _$componentController_;
      products = [
        {id: 1, name: "Hewlett-Packard Rideable Lawnmower", type: "Lawnmower", price: 3000},
        {id: 2, name: "New York Yankees T-Shirt", price: 8, type: "T-Shirt"}
      ];
    }));

    describe('changeCurrency method', function() {
      it('sets NZD "convertedPrice" on a product', function() {
        var $scope = {};
        var controller = $componentController('productTable', { $scope: $scope });

        $scope.products = products;
        $scope.currencyQuery = 'NZD';
        controller.changeCurrency();
        expect($scope.products[0].convertedPrice).toEqual(3000.00);
      });

      it('sets Euro "convertedPrice" on a product', function() {
        var $scope = {};
        var controller = $componentController('productTable', { $scope: $scope });

        $scope.products = products;
        $scope.currencyQuery = 'EURO';
        controller.changeCurrency();
        expect($scope.products[0].convertedPrice).toEqual(2010.00);
      });
    });
    describe('changeSymbol method', function() {
      it('sets Dollar "currencySymbol" on a product', function() {
        var $scope = {};
        var controller = $componentController('productTable', { $scope: $scope });

        $scope.products = products;
        $scope.currencyQuery = 'NZD';
        controller.changeCurrency();
        expect($scope.products[0].currencySymbol).toEqual("$");
      });

      it('sets Euro "currencySymbol" on a product', function() {
        var $scope = {};
        var controller = $componentController('productTable', { $scope: $scope });

        $scope.products = products;
        $scope.currencyQuery = 'EURO';
        controller.changeCurrency();
        expect($scope.products[0].currencySymbol).toEqual("€");
      });
    });

  });

});
